import React from 'react';
import Result from './Result';
import Calc from './Calc';

export default class Test extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      operation: '',
      value: ''
    };

    this.onChange = this.onChange.bind(this);
  }

  onChange(e) {

    const value = Calc.calculate(e.target.value);

    this.setState({ value: value || 'Error: see example' });
  }

  render() {
    return (
      <div className="container-fluid">
        <div className="row flex-xl-nowrap">
          <div className="col-lg-6">
            <div className="input-group">
              <input
                className="form-control"
                type="text"
                placeholder="example: 2+2"
                onChange={this.onChange}
              />
              <span className="input-group-addon">=</span>
            </div>
          </div>
          <div className="col-lg-6">
            <Result value={this.state.value} />
          </div>
        </div>
      </div>
    );
  }
}
