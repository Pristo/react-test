import React from 'react';

const Result = (props) => (
  <div className="input-group">
    <input type="text" className="form-control" value={props.value} />
  </div>
);

Result.propTypes = { value: React.PropTypes.node };
export default Result;
