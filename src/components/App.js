import React from 'react';
import { Link } from 'react-router';

const bitbucket = 'https://bitbucket.org/Pristo/react-test/overview';

const App = ({ children }) => (
  <div>
    <header>
      <h1>React Math operations</h1>
      <div className="nav">
        <Link to="/test">Test</Link>
        <a href={bitbucket} target="_blank">Bitbucket</a>
      </div>
    </header>
    <section>
      {children || <ul>
        <li>Click <a href="#/test">Test</a> to see the magic</li>
        <li>Click <a href={bitbucket} target="_blank">Bitbucket</a> for source code</li>
      </ul>}
    </section>
  </div>
);

App.propTypes = { children: React.PropTypes.object };

export default App;
