import React from 'react';

export default class Calc extends React.Component {
  static calculate(operation) {
    const rex = /^([-+]?[0-9]*\.?[0-9])([-+/*])([-+]?[0-9]*\.?[0-9])/g;
    const matches = rex.exec(operation);
    const operators = ['*', '/', '-', '+'];

    if (matches !== null) {
      if (operators.indexOf(matches[2]) > -1) {
        return eval(matches[1] + matches[2] + matches[3]);
      }
    } else {
      return false;
    }
    return true;
  }
}
